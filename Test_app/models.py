from django.db import models
from .sorters.MethodSort import MethodSortChoices


class Sort(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    edited_on = models.DateTimeField(auto_now=True)
    sort_method = models.PositiveSmallIntegerField(
        choices=MethodSortChoices, default=1)
    array = models.TextField(null=True)
    sorted_array = models.TextField(null=True)
    sorted_time = models.CharField(max_length=200, null=True)
