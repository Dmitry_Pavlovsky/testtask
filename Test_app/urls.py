from django.urls import path
from .views import CreateSortView

urlpatterns = [
    path('', CreateSortView.as_view(), name="main"),
    
]