from django import forms
from .models import Sort
from .sorters.MethodSort import MethodSortChoices


class UploadFileForm(forms.Form):

    file = forms.FileField()
    sort_method = forms.ChoiceField(choices= MethodSortChoices)

    class Meta:
        model = Sort
        fields = ('sort_method', 'file')
