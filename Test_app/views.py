from django.shortcuts import redirect, render
from django.views.generic.edit import FormView
from .forms import UploadFileForm
from .models import Sort
from  django.http import HttpResponse
from .sorters.MethodSort import MethodSortDict

class CreateSortView(FormView):
    template_name = 'main.html'
    success_url = "/"
    form_class = UploadFileForm
    success_message = "Successfully"

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            files = request.FILES.getlist('file')
            nums = []
            try:
                self._handle_files_data(files)
            except ValueError:
                return HttpResponse("There are extraneous characters in the file.") 
            except:
                return HttpResponse("Something happened. The file must contain numbers in the format (1 22 87 64 23 45).") 

            return redirect(self.success_url)
        else:
            return HttpResponse(str(form.errors))

    def get(self, request, *args, **kwargs):
        form = self.get_form(self.form_class)
        return render(request, self.template_name, {"form": form})

    def _handle_files_data(self, files):
        for file in files:
            nums += [int(x) for x in file.read().split()]
            sort_method = form.cleaned_data['sort_method']
            sorter = MethodSortDict[int(sort_method)]()
            sorted_array = sorter.sort(nums)
            Sort.objects.create(sort_method=sort_method, array=nums, sorted_array= sorted_array, sorted_time=sorter.sort.timer)
