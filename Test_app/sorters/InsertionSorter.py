from .Sorter import Sorter
from .decorators import timer
from typing import List

class InsertionSorter(Sorter):

    def __init__(self):
        pass

    @timer
    def sort(self, nums: List[int]) -> List[int]:
        for i in range(1, len(nums)):
            item_to_insert = nums[i]
            j = i - 1
            while j >= 0 and nums[j] > item_to_insert:
                nums[j + 1] = nums[j]
                j -= 1
            nums[j + 1] = item_to_insert
        
        return nums
    
