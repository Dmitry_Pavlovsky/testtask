from .Sorter import Sorter
from .decorators import timer
from typing import List

class MergeSorter(Sorter):

    def __init__(self):
        pass

    def _merge(self, left_list, right_list):  
        sorted_list = []
        left_list_index = right_list_index = 0
        
        left_list_length, right_list_length = len(left_list), len(right_list)

        for _ in range(left_list_length + right_list_length):
            if left_list_index < left_list_length and right_list_index < right_list_length:
                
                if left_list[left_list_index] <= right_list[right_list_index]:
                    sorted_list.append(left_list[left_list_index])
                    left_list_index += 1
                    
                else:
                    sorted_list.append(right_list[right_list_index])
                    right_list_index += 1

            elif left_list_index == left_list_length:
                sorted_list.append(right_list[right_list_index])
                right_list_index += 1
                
            elif right_list_index == right_list_length:
                sorted_list.append(left_list[left_list_index])
                left_list_index += 1

        return sorted_list
    
    @timer
    def sort(self, nums: List[int]):
        if len(nums) <= 1:
            return nums        
        mid = len(nums) // 2
        left_list = self.sort(nums[:mid])
        right_list = self.sort(nums[mid:])
        return self._merge(left_list, right_list)
