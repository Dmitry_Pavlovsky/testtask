from functools import wraps
import time


def timer(func):

    @wraps(func)
    def counter(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        counter.timer = '{:.2f}ms'.format(1000 * (time.time() - start_time))
        return result

    counter.timer = 0
    return counter
