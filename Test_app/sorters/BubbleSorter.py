from .Sorter import Sorter
from .decorators import timer
from typing import List

class BubbleSorter(Sorter):

    def __init__(self):
        pass

    @timer
    def sort(self, nums: List[int]) -> List[int]:
        N = len(nums)
        for i in range(N-1):
            for j in range(N-i-1):
                if nums[j] > nums[j+1]:
                    buff = nums[j]
                    nums[j] = nums[j+1]
                    nums[j+1] = buff
        
        return nums
    
