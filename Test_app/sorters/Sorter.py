from abc import ABC, abstractmethod


class Sorter(ABC):
  
  @abstractmethod
  def sort(list):
      pass