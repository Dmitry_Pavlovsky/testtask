from .MergeSorter import MergeSorter
from .BubbleSorter import BubbleSorter
from .InsertionSorter import InsertionSorter

MethodSortDict={
    1: BubbleSorter,
    2: InsertionSorter,
    3:  MergeSorter
}

MethodSortChoices=[
    (1, 'Bubble'),
    (2, 'Insertion'),
    (3, 'Merge')
]
