from django.contrib import admin
from .models import Sort

class SortAdmin(admin.ModelAdmin):
    list_display = ('sort_method', 'sorted_time', 'created_on', 'edited_on', 'array', 'sorted_array')
    list_filter = ['sort_method']
    search_fields = ['sort_method', 'sorted_time', 'created_on', 'edited_on', 'array', 'sorted_array']

admin.site.register(Sort, SortAdmin)